<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');

require_once ('admin.php');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/make-me-admin',function(){
    auth()->user()->forceFill(['is_admin'=>0])->save();
});