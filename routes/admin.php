<?php

Route::group(['prefix'=>'admin', 'middleware' => ['auth',\App\Http\Middleware\AdminAccess::class], 'namespace' => 'Admin'], function () {

    Route::get('/users', 'UsersController@index');

    Route::get('/users/create', 'UsersController@create');

    Route::post('/users/store', 'UsersController@store');

    Route::get('/users/{user}/edit', 'UsersController@edit');

    Route::post('/users/{user}', 'UsersController@update');

    Route::get('/users/{user}/delete', 'UsersController@delete');
});




//Route::resource('/admin/users/','Admin\UsersController');
