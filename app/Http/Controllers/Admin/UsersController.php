<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $users = User::search($request->get('searchQuery'))->paginate(10);

        return view('admin.users.index', ['listUsers' => $users] );
    }

    public function create(Request $request)
    {
        return view('admin.users.create');
    }

    public function edit(Request $request, User $user)
    {
//        $user = User::findOrFail($request->route('user'));

        return view('admin.users.edit',['user' => $user]);
    }

    public function store(UserRequest $request)
    {
        $user = new User();

        $user->fill([
            'name'     => $request->get('name'),
            'email'    => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        $user->save();

        return redirect()->to('/admin/users/');
    }

    public function update(UserRequest $request, User $user)
    {
        $user->fill([
            'name'     => $request->get('name'),
            'email'    => $request->get('email'),
        ]);

        if($request->get('password')){
            $user->password = bcrypt($request->get('password'));
        }

        $user->save();

        return redirect()->to('/admin/users/');
    }

    public function delete(Request $request, User $user)
    {
        $user->delete();

        if($request->expectsJson()){
            return response()->json(['status'=>true]);
        }

        return redirect()->to('/admin/users/');
    }

}
