<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * TODO password confirmation
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rulesUniqueEmail = 'unique:users,email'.
            ( $this->route('user') ? (','.$this->route('user')->id) : '' );

        $passwordRules = $this->route('user') ? ['nullable', 'min:6'] : ['required', 'min:6'];

        return [
            'name'     => [
                'required', 'string'
            ],
            'email'    => [
                'required', 'email', $rulesUniqueEmail
            ],
            'password' => $passwordRules,
        ];
    }
}
