<div class="form-group">
    <label for="email-input-users">Email address</label>
    <input type="text" value="{{ $user->email ?? request()->old('email') }}" name="email" class="form-control" id="email-input-users" placeholder="Enter email">
</div>

<div class="form-group">
    <label for="name-input-users">Name</label>
    <input type="text" value="{{ $user->name ?? request()->old('name') }}"  name="name" class="form-control" id="name-input-users" placeholder="Enter email">
</div>

<div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="text" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
</div>


{{--TODO вывести ошибки под соответсвующими полями--}}

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif